terraform {

  required_version = ">=1.0.0"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.10.0"
    }
  }

  backend "azurerm" {}
}

provider "azurerm" {
  features {}
}

variable "app_service_name" {
  default = ""
}

resource "azurerm_resource_group" "azure-app-service" {
  name     = "azure-app-service"
  location = "East US"
}

resource "azurerm_service_plan" "gitlab-app-service-plan" {
  name                = "gitlab-app-service-plan"
  location            = azurerm_resource_group.azure-app-service.location
  resource_group_name = azurerm_resource_group.azure-app-service.name
  os_type             = "Linux"
  sku_name            = "F1"
}

resource "azurerm_linux_web_app" "gitlab-app-service" {
  name                = var.app_service_name
  location            = azurerm_resource_group.azure-app-service.location
  resource_group_name = azurerm_resource_group.azure-app-service.name
  service_plan_id     = azurerm_service_plan.gitlab-app-service-plan.id
  
  app_settings = {
    "SCM_DO_BUILD_DURING_DEPLOYMENT" = "True"
  }

  site_config {
    application_stack {
      dotnet_version = "6.0"
    }
    always_on = false
  }
}

output "website_url" {
  value = azurerm_linux_web_app.gitlab-app-service.default_hostname
}
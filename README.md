# Azure App Service - dotnet

## Documentação Terraform:

- [Provider Azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs)
- [Backend Azurerm](https://www.terraform.io/language/settings/backends/azurerm)
- [Resource azurerm_resource_group](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group)
- [Resource azurerm_service_plan](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/service_plan)
- [Resource azurerm_linux_web_app](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/linux_web_app)

## Documentação Microsoft:

- [Tutorial Deploy um web app ASP.NET](https://docs.microsoft.com/pt-br/azure/app-service/quickstart-dotnetcore?tabs=net60&pivots=development-environment-cli)
- [Comando az login com Service Principal](https://docs.microsoft.com/pt-br/cli/azure/authenticate-azure-cli#sign-in-with-a-service-principal)
- [Comando az group create](https://docs.microsoft.com/en-us/cli/azure/group?view=azure-cli-latest#az-group-delete)
- [Comando az storage account create](https://docs.microsoft.com/en-us/cli/azure/storage/account?view=azure-cli-latest#az-storage-account-create)
- [Comando az storage container create](https://docs.microsoft.com/en-us/cli/azure/storage/container?view=azure-cli-latest#az-storage-container-create)
- [Comando az webapp up](https://docs.microsoft.com/en-us/cli/azure/webapp?view=azure-cli-latest#az-webapp-up)

## Documentação DockerHub:

- [Imagem Terraform](https://hub.docker.com/r/hashicorp/terraform/)
- [Imagem Microsoft](https://hub.docker.com/_/microsoft-azure-cli)